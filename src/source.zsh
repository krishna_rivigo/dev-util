# vim: ft=zsh
# This script is not intended to be sourced manually.
# Please run installation script 'install.sh'

# Source custom scripts
for file in ~/.dev/scripts/*; do
    source "$file"
done

# Directory path to autoload functions
fpath=(~/.dev/functions $fpath)

autoload mkuser
autoload ae
autoload cns
autoload build ecs-build prod-build
autoload console-out build-console-out last-console-out
