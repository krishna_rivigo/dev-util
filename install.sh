#!/usr/bin/env sh

if [ ! -e ./LICENSE ]
then
    echo "This is not a valid copy of the project. Please contact the original authors for a Genuine copy."
    exit 1
fi

cat LICENSE

echo "\nDo you agree to the license above (y/n)? "
read agree

if [ "$agree" == "${agree#[Yy]}" ]
then
    echo "Not installing the scripts. Bye!!"
    exit 2
fi

if [ -e ~/.dev ]
then
    cat << EOF
There is already a file or directory present at '~/.dev'.
This installation script creates a symlink at that location.
I'm sorry! I can't continue until you remove whatever that is.
EOF
    exit 1
fi

which jq > /dev/null
if [ "$?" -ne 0 ]
then
    echo "Installing dependency: jq"
    brew install jq
fi

a="/$0"; a=${a%/*}; a=${a#/}; a=${a:-.}; BASEDIR=$(cd "$a"; pwd)

ln -s "$BASEDIR/src" ~/.dev
test -f "~/.dev" && echo "Created ~/.dev directory"
cat << EOF >> ~/.zshrc
source ~/.dev/source.zsh
EOF
echo 'Done installing Dev Utils. Use with caution. You may want to read the LICENSE!!!'
