# DEV Utils

These are just a few scripts the author uses in his day to day work. Hope you
find them useful.

Note: These scripts are known to work only on ZSH.

Installing these scripts will add some functions and environment variables to your ZSH.

# Installation

Clone this project and run `$ ./install.sh` from project root inside a zsh shell.

All the configuration lies in directory `src/scripts/`

# System naming scheme

Following naming scheme of systems is used for most of the commands

```
SYSTEM in all the following commands refers to a service, environment and
a sub-project (if applicable).

This is the naming scheme of systems.
XY for api, XYZ for scheduler
	X - Environment ('d' for dev, etc.)
	Y - Service ('c' for crm, etc.)
	Z - api/scheduler; none for api, 's' for scheduler

Scheme for environment naming
d Dev
s Staging
p Prod

Service naming
c CRM
p POD
o OMS
k Calling
s DataSource
f Supply(fleet)
d Demand

For example,
  'dc' - dev crm
  'sds' - staging demand scheduler
Suppose you want to invoke command to deploy dev crm,
you do that with `$ build dc <branch-name>`
```

# Jenkins

You can build jenkins jobs from your command line. Only prerequisite is to
generate an API token. See the next section to know how.

Configure jenkins for your account at [Jenkins Config file](src/scripts/jenkins)
to start building jenkins jobs from your command line.
There are a few pre-configured systems such as CRM, Supply, etc. You can easily
configure for other systems too.

Note: each of the build commands displays console output also.

## Jenkins commands

### `$ build SYSTEM BRANCH`

This is used to build a job which is non prod and non ecs.
See file `src/functions/build` to understand more.

```zsh
$ build dc staging      # build staging branch on dev CRM
$ build df common-dev   # build common-dev branch on dev Supply
```

### `$ ecs-build SYSTEM COMMAND BRANCH`

This is used to build an ecs job.
See file `src/functions/ecs-build` to understand more.

```zsh
$ ecs-build sc make staging      # build 'staging' branch on staging CRM
$ ecs-build sc del test          # remove 'test' branch from staging CRM
$ ecs-build sds make staging     # build 'staging' branch on staging Demand Scheduler
```

### `$ prod-build SYSTEM RELEASE_TAG`

This is used to build a production job.
See file `src/functions/prod-build` to understand more.

```zsh
$ prod-build pc green-indent  # Release CRM to production with tag 'green-indent'
$ prod-build pd gst-new       # Release Demand API to production with tag 'gst-new'
```

### `$ mkuser PRIVATE_IP`

Create SSH user in the machine with the given ip address.
You need to configure ssh username in `src/scripts/config` file.

## How to create Jenkins API Token

Just run this curl with your user id and password. And the value of "tokenValue"
field from the response is your newly created API token. The command will create
a new API token with name 'Job Build'. You can see the names of all the created
tokens by logging in to Jenkins.

```zsh
curl 'https://jenkins.vyom.com/jenkins/me/descriptorByName/jenkins.security.ApiTokenProperty/generateNewToken' --data 'newTokenName=Job Build' --user '<jenkins-user-id>:<jenkins-password>'
```

# Consul

### `$ cns SYSTEM`

Gets the current ecs deployments of the system. Systems for consul can be
configured in the file `src/scripts/consul`

```zsh
$ cns sc
Deployments of vyom-crm-staging
10.0.3.93	32914	advance-vas-vyom-crm-staging
10.0.3.93	32915	pricing-vyom-crm-staging
10.0.3.93	32918	staging-vyom-crm-staging

$ cns sf
Deployments of supply-staging-api
10.0.1.36	33037	staging-supply-staging-api
```

# AWS

You need `aws-cli` installed and configured for these commands to work.

### `$ ae SEARCH_TERM`

Searches names of ec2 instances for the SEARCH_TERM substring.

```zsh
$ ae crm
i-09306d7e85bbd92b4	vyom-crm-staging-ecs
IP	10.0.3.93
i-08ee7f0afbec59fbb	vyom-crm-staging-ecs
IP	10.0.3.23
i-0e67475044b1ba203	vyom-crm-backend-prod-v2
IP	172.16.53.244

$ ae pod
i-0922a27d37361cf97	vyom-pod-prod-v1
IP	172.16.49.219
```

